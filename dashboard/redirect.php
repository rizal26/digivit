<?php
$page = (isset($_GET['page']))? $_GET['page'] : '';

switch($page){
  case 'dashboard': 
  include "content/dashboard.php"; 
  break;

  case 'guest-list': 
  include "content/guest-list.php"; 
  break;
  
  case 'event-list': 
  include "content/event-list.php"; 
  break;
  
  case 'create-event':
  include "content/create-event.php"; 
  break;
  
  default: 
  include "content/dashboard.php";
}
?>