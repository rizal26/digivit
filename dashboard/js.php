    
    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <script src="app-assets/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>

    <!-- END: Page Vendor JS-->
    
    <script>
        $(function () {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            };

            // console.log(getUrlParameter('page'));
            if (getUrlParameter('page') == 'dashboard' || getUrlParameter('page') == '') {
                $('#dashboard').addClass('active');
            } else if (getUrlParameter('page') == 'guest-list') {
                $('#guest-list').addClass('active');
                $('#main-menu').addClass('active');
            } else if (getUrlParameter('page') == 'event-list') {
                $('#event-list').addClass('active');
                $('#main-menu').addClass('active');
            } else {
                $('#create-event').addClass('active');
                $('#main-menu').addClass('active');
            }
        });
    </script>