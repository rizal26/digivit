    
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="row" style="width: 100%; padding-bottom: 10px;">
                                        <div class="col-12 text-center">
                                            <h4 class="text-bold-700">Finished Events</h4>
                                        </div>
                                    </div>
                                    <div class="row" style="width: 100%;">
                                        <div class="col-2" style="padding-top: 24px;">
                                            <div class="avatar bg-rgba-primary p-50 m-0">
                                                <div class="avatar-content">
                                                    <i class="feather icon-monitor text-primary font-medium-5"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-10">
                                            <h2 class="text-bold-700 mt-2 mb-25">100</h2>
                                            <p class="mb-0">Total Finished Event</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div id="subscribe-gain-chart-first"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="row" style="width: 100%; padding-bottom: 10px;">
                                        <div class="col-12 text-center">
                                            <h4 class="text-bold-700">Future Events</h4>
                                        </div>
                                    </div>
                                    <div class="row" style="width: 100%;">
                                        <div class="col-2" style="padding-top: 24px;">
                                            <div class="avatar bg-rgba-primary p-50 m-0">
                                                <div class="avatar-content">
                                                    <i class="feather icon-monitor text-primary font-medium-5"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-10">
                                            <h2 class="text-bold-700 mt-2 mb-25">100</h2>
                                            <p class="mb-0">Total Future Event</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div id="subscribe-gain-chart-second"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="row" style="width: 100%; padding-bottom: 10px;">
                                        <div class="col-12 text-center">
                                            <h4 class="text-bold-700">On Going Events</h4>
                                        </div>
                                    </div>
                                    <div class="row" style="width: 100%;">
                                        <div class="col-2" style="padding-top: 24px;">
                                            <div class="avatar bg-rgba-primary p-50 m-0">
                                                <div class="avatar-content">
                                                    <i class="feather icon-monitor text-primary font-medium-5"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-10">
                                            <h2 class="text-bold-700 mt-2 mb-25">100</h2>
                                            <p class="mb-0">Total On Going Event</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div id="subscribe-gain-chart"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0">History</h4>
                                </div>
                                <div class="card-content">
                                    <div class="table-responsive mt-1">
                                        <table class="table mb-0">
                                                <tr>
                                                    <td><button type="button" class="btn btn-primary btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-success mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-warning badge-glow mr-1 mb-1">On Progress</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="button" class="btn btn-success btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Cordova & Alaska</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-warning mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-danger badge-glow mr-1 mb-1">Finish</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="button" class="btn btn-warning btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Florence & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-success mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Finish</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="button" class="btn btn-danger btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Clifton & Arizona</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-danger mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Finish</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="button" class="btn btn-secondary btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Clifton & Arizona</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-danger mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Finish</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="button" class="btn btn-warning btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Clifton & Arizona</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-danger mr-50"></i>400</td>
                                                    <td>
                                                        <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Finish</div>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:voi(0)"><i class="feather icon-download" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                        <a href="javascript:voi(0)"><i class="feather icon-more-vertical" style="padding-right: 20px;"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-1">Total Events</h4>
                                </div>
                                <div class="card-content">
                                    <div id="goal-overview-chart" class="mt-75" style="margin-bottom: -24px;"></div>
                                    <section class="text-center" style="margin-left: 10px;">
                                        <ul class="activity-timeline timeline-left list-unstyled" 
                                        style="height: 16rem;margin-bottom: 37px;padding-left: 47px;border-left: 7px solid #dae1e7;margin-left: 3.5rem; width: 85%;">
                                            <li style="height: 50px; padding-left: 15%;">
                                                <div class="timeline-icon bg-primary">
                                                    <i class="font-medium-2 align-middle"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <span class="font-weight-bold mb-0">January</span>
                                                        </div>
                                                        <div class="col-6">
                                                            <span class="font-weight-bold">100</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <small class=""></small> -->
                                            </li>
                                            <li style="height: 50px; padding-left: 15%;">
                                                <div class="timeline-icon bg-warning">
                                                    <i class="font-medium-2 align-middle"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <span class="font-weight-bold mb-0">February</span>
                                                        </div>
                                                        <div class="col-6">
                                                            <span class="font-weight-bold">100</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <small class=""></small> -->
                                            </li>
                                            <li style="height: 50px; padding-left: 15%;">
                                                <div class="timeline-icon bg-danger">
                                                    <i class="font-medium-2 align-middle"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <span class="font-weight-bold mb-0">March</span>
                                                        </div>
                                                        <div class="col-6">
                                                            <span class="font-weight-bold">50</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <small class=""></small> -->
                                            </li>
                                            <li style="height: 50px; padding-left: 15%;">
                                                <div class="timeline-icon bg-success">
                                                    <i class="font-medium-2 align-middle"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <span class="font-weight-bold mb-0">April</span>
                                                        </div>
                                                        <div class="col-6">
                                                            <span class="font-weight-bold">50</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <small class=""></small> -->
                                            </li>
                                        </ul>
                                    </section>
                                </div>
                            </div>
                    </div>

                    <!-- BEGIN: Page Vendor JS-->
                    <script src="app-assets/vendors/js/charts/apexcharts.js"></script>
                    <!-- END: Page Vendor JS-->

                    <!-- BEGIN: Page JS-->
                    <script src="app-assets/js/scripts/pages/dashboard-analytics.js"></script>
                    <script src="app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
                    <!-- END: Page JS-->
                