                        <style>
                            .tbl-icon{
                                text-align: end;
                                width: 70px;
                                font-size: 35px;
                            }
                            .tbl-note {
                                width: 200px;
                            }
                            .tbl-center{
                                text-align: center
                            }
                        </style>
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="table-responsive mt-0">
                                            <table class="table table-borderless mb-0">
                                                <tr>
                                                    <td style="width: 100px;"><button type="button" class="btn btn-warning btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                    <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                    <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                    <td><i class="feather icon-users font-small-3 text-secondary mr-50"></i>400<br>
                                                        <i class="feather icon-users font-small-3 text-success mr-50"></i>400</td>
                                                    <td>
                                                    <td class="tbl-icon">
                                                        <i class="feather icon-user"></i>
                                                    </td>
                                                    <td>
                                                        <b>Jordy</b><br>
                                                        081234567890
                                                    </td>
                                                    <td class="tbl-icon">
                                                        <i class="feather icon-file-text"></i>
                                                    </td>
                                                    <td class="tbl-note">
                                                        <span class="text-muted">Panitia penerima tamu untuk stand by 15 menit sblum acara dimulai</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-9">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-md-12 text-center">
                                            <h3 class="text-bold-700">Guest List</h3>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <form>
                                                <div class="row col-lg-12 text-center" style="padding-left: 40px;">
                                                    <div class="col-lg-2"><input type="text" class="form-control" placeholder="Name"></div>
                                                    <div class="col-lg-2"><input type="text" class="form-control" placeholder="Phone Number"></div>
                                                    <div class="col-lg-2"><input type="text" class="form-control" placeholder="E-mail"></div>
                                                    <div class="col-lg-2"><input type="text" class="form-control" placeholder="From"></div>
                                                    <div class="col-lg-2"><input type="text" class="form-control" placeholder="Guest Type"></div>
                                                    <div class="col-lg-2"><button type="button" class="btn btn-primary" id="subimit">Submit</button></div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive mt-1">
                                                <table class="table" id="tbl-guest-list">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Name</th>
                                                            <th>Phone Number</th>
                                                            <th>E-mail</th>
                                                            <th>From</th>
                                                            <th>Guest Type</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Ridho</td>
                                                            <td>081234567890</td>
                                                            <td>ridho@angkasa.com</td>
                                                            <td>BRM</td>
                                                            <td>VIP</td>
                                                            <td>
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Ridho</td>
                                                            <td>081234567890</td>
                                                            <td>ridho@angkasa.com</td>
                                                            <td>BRM</td>
                                                            <td>VIP</td>
                                                            <td>
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Ridho</td>
                                                            <td>081234567890</td>
                                                            <td>ridho@angkasa.com</td>
                                                            <td>BRM</td>
                                                            <td>VIP</td>
                                                            <td>
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Ridho</td>
                                                            <td>081234567890</td>
                                                            <td>ridho@angkasa.com</td>
                                                            <td>BRM</td>
                                                            <td>VIP</td>
                                                            <td>
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="card">
                                    <div class="card-header" style="padding-bottom: 20px">
                                        <div class="col-12 text-center">
                                            <h3 class="text-bold-700">Total Submited</h3>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div id="total-submited"></div>
                                        </div>
                                    </div>
                                </div>        
                            </div>
                        </div>

                        <!-- BEGIN: Page Vendor JS-->
                        <script src="app-assets/vendors/js/charts/apexcharts.js"></script>
                        <!-- END: Page Vendor JS-->

                        <script>
                            $(function () {
                                $('#tbl-guest-list').DataTable();

                                var $primary = '#7367F0';
                                var $success = '#28C76F';
                                var $danger = '#EA5455';
                                var $warning = '#FF9F43';
                                var $info = '#00cfe8';
                                var $primary_light = '#A9A2F6';
                                var $danger_light = '#f29292';
                                var $success_light = '#55DD92';
                                var $warning_light = '#ffc085';
                                var $info_light = '#1fcadb';
                                var $strok_color = '#b9c3cd';
                                var $label_color = '#e7e7e7';
                                var $white = '#fff';
                                var val = 75;
                                var goalChartoptions = {
                                    chart: {
                                    height: 250,
                                    type: 'radialBar',
                                    sparkline: {
                                        enabled: true,
                                    },
                                    dropShadow: {
                                        enabled: true,
                                        blur: 3,
                                        left: 1,
                                        top: 1,
                                        opacity: 0.1
                                    },
                                    },
                                    colors: [$success],
                                    plotOptions: {
                                    radialBar: {
                                        hollow: {
                                        size: '77%',
                                        },
                                        track: {
                                        background: $strok_color,
                                        strokeWidth: '50%',
                                        },
                                        dataLabels: {
                                        name: {
                                            show: true,
                                            fontSize: '16px',
                                            color: $strok_color,
                                            offsetY: 30
                                        },
                                        value: {
                                            offsetY: -10,
                                            color: $success,
                                            fontSize: '3rem',
                                            show: true,
                                        }
                                        }
                                    }
                                    },
                                    fill: {
                                    type: 'gradient',
                                    gradient: {
                                        shade: 'dark',
                                        type: 'horizontal',
                                        shadeIntensity: 0.5,
                                        gradientToColors: ['#00b5b5'],
                                        inverseColors: true,
                                        opacityFrom: 1,
                                        opacityTo: 1,
                                        stops: [0, 100]
                                    },
                                    },
                                    labels: ['300/400'],
                                    series: [val],
                                    stroke: {
                                        lineCap: 'round'
                                    },
                                }

                                var goalChart = new ApexCharts(
                                    document.querySelector("#total-submited"),
                                    goalChartoptions
                                );

                                goalChart.render();
                            });
                        </script>

                        