                        <style>
                            .tbl-icon{
                                text-align: end;
                                width: 70px;
                                font-size: 35px;
                            }
                            .tbl-note {
                                width: 200px;
                            }
                            .tbl-center{
                                text-align: center
                            }
                        </style>
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header" style="padding-bottom: 20px">
                                        <div class="col-12 text-center">
                                            <h3 class="text-bold-700">Event List</h3>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                        <tr>
                                                            <td style="width: 100px;"><button type="button" class="btn btn-primary btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                            <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                            <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-user"></i>
                                                            </td>
                                                            <td>
                                                                <b>Jordy</b><br>
                                                                081234567890
                                                            </td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-file-text"></i>
                                                            </td>
                                                            <td class="tbl-note">
                                                                <span class="text-muted">Panitia penerima tamu untuk stand by 15 menit sblum acara dimulai</span>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Success</div>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;"><button type="button" class="btn btn-warning btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                            <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                            <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-user"></i>
                                                            </td>
                                                            <td>
                                                                <b>Jordy</b><br>
                                                                081234567890
                                                            </td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-file-text"></i>
                                                            </td>
                                                            <td class="tbl-note">
                                                                <span class="text-muted">Panitia penerima tamu untuk stand by 15 menit sblum acara dimulai</span>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Success</div>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;"><button type="button" class="btn btn-success btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                            <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                            <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-user"></i>
                                                            </td>
                                                            <td>
                                                                <b>Jordy</b><br>
                                                                081234567890
                                                            </td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-file-text"></i>
                                                            </td>
                                                            <td class="tbl-note">
                                                                <span class="text-muted">Panitia penerima tamu untuk stand by 15 menit sblum acara dimulai</span>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Success</div>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;"><button type="button" class="btn btn-danger btn-lg waves-effect waves-light" style="padding: 16px">RK</button></td>
                                                            <td><b>Anniston & Alabama</b><br><small class="text-mute">Menara 165</small></td>
                                                            <td><b>26/07/2018</b><br><small class="text-muted">11:00 - 13:00</small></td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-user"></i>
                                                            </td>
                                                            <td>
                                                                <b>Jordy</b><br>
                                                                081234567890
                                                            </td>
                                                            <td class="tbl-icon">
                                                                <i class="feather icon-file-text"></i>
                                                            </td>
                                                            <td class="tbl-note">
                                                                <span class="text-muted">Panitia penerima tamu untuk stand by 15 menit sblum acara dimulai</span>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <div class="badge badge-pill badge-success badge-glow mr-1 mb-1">Success</div>
                                                            </td>
                                                            <td class="tbl-center">
                                                                <a href="javascript:voi(0)"><i class="feather icon-edit-2" style="padding-right: 20px;"></i></a>
                                                                <a href="javascript:voi(0)"><i class="feather icon-trash" style="padding-right: 20px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                    <div class="col-12 text-center">
                                        <a href="javascript:void(0)" class="btn btn-primary">Create New</a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>