                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="#" class="number-tab-steps wizard-circle">
                                            <!-- Step 1 -->
                                            <h6>Step 1</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="eventName">Event Name </label>
                                                            <input type="text" class="form-control" id="eventName" placeholder="Event Name">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="dateEvent">Date of Event</label>
                                                            <input class="form-control" id="datepicker" placeholder="Date of Event" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="picName">PIC Name</label>
                                                            <input type="text" class="form-control" id="picName" placeholder="PIC Name">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="beginAt">Begin At</label>
                                                            <input type="text" class="form-control" id="beginAt" placeholder="Begin At">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="picPhone">PIC Phone Number</label>
                                                            <input type="text" class="form-control" id="picPhone" placeholder="PIC Phone Number">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="endAt">End At</label>
                                                            <input type="text" class="form-control" id="endAt" placeholder="End At">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="location">Location</label>
                                                            <input type="text" class="form-control" id="location" placeholder="Location">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="totalCap">Total Capacity</label>
                                                            <input type="text" class="form-control" id="totalCap" placeholder="Total Capacity">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <!-- Step 2 -->
                                            <h6>Step 2</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="locationUrl">Location URL</label>
                                                            <input type="text" class="form-control" id="locationUrl" placeholder="Location URL">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="videoUrl">Video URL</label>
                                                            <input type="text" class="form-control" id="videoUrl" placeholder="Video URL">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="igLink">Instagram Link</label>
                                                            <input type="text" class="form-control" id="igLink" placeholder="Instagram Link">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="undangan">Upload Undangan</label>
                                                            <input type="file" class="form-control" id="undangan" placeholder="Upload Undangan">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea id="notes" rows="4" class="form-control" placeholder="Notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <!-- Step 3 -->
                                            <h6>Step 3</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="eventName">Event Name </label>
                                                            <input type="text" class="form-control" readonly placeholder="Event Name">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="dateEvent">Date of Event</label>
                                                            <input type="text" class="form-control" readonly placeholder="Date of Event">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="picName">PIC Name</label>
                                                            <input type="text" class="form-control" readonly placeholder="PIC Name">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="beginAt">Begin At</label>
                                                            <input type="text" class="form-control" readonly placeholder="Begin At">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="picPhone">PIC Phone Number</label>
                                                            <input type="text" class="form-control" readonly placeholder="PIC Phone Number">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="endAt">End At</label>
                                                            <input type="text" class="form-control" readonly placeholder="End At">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="location">Location</label>
                                                            <input type="text" class="form-control" readonly placeholder="Location">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="totalCap">Total Capacity</label>
                                                            <input type="text" class="form-control" readonly placeholder="Total Capacity">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="locationUrl">Location URL</label>
                                                            <input type="text" class="form-control" readonly placeholder="Location URL">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="videoUrl">Video URL</label>
                                                            <input type="text" class="form-control" readonly placeholder="Video URL">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="igLink">Instagram Link</label>
                                                            <input type="text" class="form-control" readonly placeholder="Instagram Link">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="undangan">Upload Undangan</label>
                                                            <input type="text" class="form-control" readonly placeholder="Upload Undangan">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="notes">Notes</label>
                                                            <input type="text" class="form-control" readonly placeholder="Notes">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
                    <script src="app-assets/js/scripts/forms/wizard-steps.js"></script>
                    <script src="app-assets/js/scripts/gijgo/gijgo.min.js" type="text/javascript"></script>
                    <script>
                        $('#datepicker').datepicker({
                            uiLibrary: 'bootstrap4'
                        });
                    </script>